# Install

## Prerequisites

1. Execute prerequisites.sh with sudo

prerequisites.sh

## Steps

1. Clone this repository

git clone https://traveleala@bitbucket.org/traveleala/traveleala.git --recurse-submodules

2. Exec docker

cd /var/www/traveleala
docker-compose build && docker-compose up

3. Execute the install.sh

bash install.sh

# Develop

## Steps

1. Execute develop.sh

   bash develop.sh
