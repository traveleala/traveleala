##################
# Backend
##################

cd ./backend/

# Checkout develop
git checkout develop

# Update .env
cp .env.example .env

# Install libraries
composer install
composer dump-autoload

# Permissions
sudo chown -R www-data:www-data storage/
sudo chown -R www-data:www-data vendor/
sudo chmod -R 755 ./storage

# Restart supervisor
sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl start traveleala-worker:\*

cd ..

##################
# Frontend
##################

cd ./frontend/

# Checkout develop
git checkout develop

# Execute server
npm i