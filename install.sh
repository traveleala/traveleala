##################
# Frontend
##################

cd ./frontend/

# Checkout develop
git checkout develop

# Install dependencies
npm install
cd ..

##################
# Backend
##################

cd ./backend/

# Checkout develop
git checkout develop

# Copy .env file
cp .env.example .env

# Composer
composer install
composer dump-autoload

# Add hosts in etc/hosts
sudo -- sh -c "grep -qxF '127.0.0.1 traveleala.org' /etc/hosts || echo '127.0.0.1 traveleala.org' >> /etc/hosts"
sudo -- sh -c "grep -qxF '127.0.0.1 api.traveleala.org' /etc/hosts || echo '127.0.0.1 api.traveleala.org' >> /etc/hosts"

# Permissions
sudo chown -R www-data:www-data storage/
sudo chown -R www-data:www-data vendor/
sudo chmod -R 755 ./storage

# Move to folder conf
cd readmes/conf

# Copy ssl
sudo -- sh -c "rm -rf /etc/ssl/traveleala"
sudo -- sh -c "mkdir /etc/ssl/traveleala"
sudo -- sh -c "cp ./traveleala.crt /etc/ssl/traveleala"
sudo -- sh -c "cp ./traveleala.key /etc/ssl/traveleala"

# Nginx conf
sudo -- sh -c "rm -rf /etc/nginx/sites-available/traveleala.conf"
sudo -- sh -c "cp ./traveleala.conf /etc/nginx/sites-available/"

# Symbolic link
sudo -- sh -c "rm -rf /etc/nginx/sites-enabled/traveleala.conf"
sudo -- sh -c "ln -s /etc/nginx/sites-available/traveleala.conf /etc/nginx/sites-enabled/traveleala.conf"

# Restar nginx
sudo service nginx restart

# Supervisor conf
sudo -- sh -c "rm -rf /etc/supervisor/conf.d/traveleala-worker.conf"
sudo cp ./supervisor.conf /etc/supervisor/conf.d/traveleala-worker.conf

# Restart supervisor
sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl start traveleala-worker:\*

# Add to cronjob
(crontab -l ; echo "* * * * * cd /var/www/traveleala/backend && php artisan schedule:run >> /dev/null 2>&1") 2>&1 | grep -v "no crontab" | sort | uniq | crontab -